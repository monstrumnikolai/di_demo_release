using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "UntitledInstaller", menuName = "Installers/ScriptableObjectInstallerTest")]
public class ScriptableObjectInstaller : ScriptableObjectInstaller<ScriptableObjectInstaller>
{
    [SerializeField] private int testInteger;
    [SerializeField] private float testFloat;
    [SerializeField] private GameObject testPrefab;
    
    public override void InstallBindings()
    {
        Container.Bind<int>().FromInstance(testInteger);
        Container.Bind<float>().FromInstance(testFloat);
        Container.Bind<GameObject>().FromInstance(testPrefab);
    }
}