﻿using System.Collections;
using System.Collections.Generic;
using DIDemo.MovieExample;
using EasyButtons;
using UnityEngine;
using Zenject;

public class MovieInjectionTest : MonoBehaviour
{
    private MovieCustomer _movieCustomer;
    private string _movieToRequest;

    [Inject]
    public void Construct(MovieCustomer movieCustomer, string moviename)
    {
        _movieCustomer = movieCustomer;
        _movieToRequest = moviename;
        Debug.Log("Injected " + movieCustomer +" as Customer and " + moviename + " as the movie to request.");
    }
    
    [Button]
    public void TestMovieRequest()
    {
        _movieCustomer.RequestMovie(_movieToRequest);
    }
}
