﻿using System.Collections;
using System.Collections.Generic;
using DIDemo.MovieExample;
using UnityEngine;
using Zenject;

public class MovieInstaller : MonoInstaller
{
    [SerializeField] private string movieName;

    public override void InstallBindings()
    {
        Container.Bind<MovieCustomer>().AsSingle(); // Erzeuge einen MovieCustomer und verwende ihn überall, wo ein MovieCustomer im Konstruktor oder bei [Inject] benötigt wird
        
        Container.Bind<IMovieStore>().To<Netflix>().AsSingle();
        
        Container.Bind<IMovieDatabaseService>().To<MovieXMLDatabaseService>().AsSingle();
        
        Container.Bind<string>().FromInstance(movieName); // Immer, wenn ein String gebraucht wird, verwende die Instanz movieName

    }
}

