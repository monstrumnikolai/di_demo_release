﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using Zenject;

public class GameObjectInjectionTest : MonoBehaviour
{
    [Inject] public GameObject testObjectField;

    [Inject]
    public GameObject TestObjectProperty
    {
        get
        {
            return _testObjectProperty;
        }

        set
        {
            _testObjectProperty = value;
            Debug.Log("Property was set to: " + value);
        }
    }

    private GameObject _testObjectProperty;


    [Inject]
    public void GameObjectInjection(GameObject gOToInject)
    {
        Debug.Log(gOToInject + " was injected into a function.");
    }
}
