﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameObjectTestInstaller : MonoInstaller
{
    [SerializeField] private GameObject _gameObject;
    
    public override void InstallBindings()
    {
        Container.Bind<GameObject>().FromInstance(_gameObject);
    }
}
