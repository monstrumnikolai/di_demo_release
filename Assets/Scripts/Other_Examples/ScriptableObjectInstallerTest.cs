﻿using System.Collections;
using System.Collections.Generic;
using DIDemo.MovieExample;
using UnityEngine;
using Zenject;

public class ScriptableObjectInstallerTest : MonoBehaviour
{
    
    [Inject]
    public void ShowTestInt(int testInt)
    {
        Debug.Log(testInt);
    }

    [Inject]
    public void ShowTestFloat(float testfloat)
    {
        Debug.Log(testfloat);
    }

    [Inject]
    public void ShowTestGameObject(GameObject testGameObject)
    {
        Debug.Log(testGameObject);
    }

}
