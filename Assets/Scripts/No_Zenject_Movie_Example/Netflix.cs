﻿// ------------------------------------------------------------------------------------------------------
// <copyright file="Netflix.cs">
// Copyright (c) 2021 Nikolai Reinke. All rights reserved.
// </copyright>
// <author>
//   Nikolai Reinke
// </author>
// ------------------------------------------------------------------------------------------------------

using UnityEngine;

namespace DIDemo.MovieExample
{
    
    public class Netflix : IMovieStore
    {
        private IMovieDatabaseService _iMovieDatabaseService;

        public Netflix(IMovieDatabaseService iMovieDatabaseService)
        {
            _iMovieDatabaseService = iMovieDatabaseService;
        }

        public void GetMovieByName(string movieName)
        {
            Debug.Log("Netflix was asked for movie " + movieName);
            _iMovieDatabaseService.FetchMovie(movieName);
        }
    }
}

