﻿// ------------------------------------------------------------------------------------------------------
// <copyright file="IMovieFetchService.cs">
// Copyright (c) 2021 Nikolai Reinke. All rights reserved.
// </copyright>
// <author>
//   Nikolai Reinke
// </author>
// ------------------------------------------------------------------------------------------------------

namespace DIDemo.MovieExample
{
    public interface IMovieDatabaseService
    {
        void FetchMovie(string movieName);
    }
}