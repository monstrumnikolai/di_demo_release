﻿namespace DIDemo.MovieExample
{
    public interface ICustomer
    {
        void RequestMovie(string movieName);
    }
}