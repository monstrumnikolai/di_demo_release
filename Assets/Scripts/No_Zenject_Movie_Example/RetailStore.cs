﻿using UnityEngine;

namespace DIDemo.MovieExample
{
    public class RetailStore : IMovieStore
    {
        private IMovieDatabaseService _iMovieDatabaseService;

        public RetailStore(IMovieDatabaseService iMovieDatabaseService)
        {
            _iMovieDatabaseService = iMovieDatabaseService;
        }

        public void GetMovieByName(string movieName)
        {
            Debug.Log("Retail store was asked for movie " + movieName);
            _iMovieDatabaseService.FetchMovie(movieName);
        }
    }
}