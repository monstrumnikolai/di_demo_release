﻿using System.Collections;
using System.Collections.Generic;
using DIDemo.MovieExample;
using UnityEngine;

namespace DIDemo.MovieExample
{
    public class MovieObjectGraphRoot : MonoBehaviour
    {
        void Start()
        {
            // Create all objects necessary to fulfill a given task at the root of the dependency graph
            MovieCustomer testCustomer = new MovieCustomer(new Netflix(new MovieXMLDatabaseService()));
        }
    }
}