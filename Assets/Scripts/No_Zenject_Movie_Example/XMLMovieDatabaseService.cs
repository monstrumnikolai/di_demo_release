﻿// ------------------------------------------------------------------------------------------------------
// <copyright file="XMLMovieFetchService.cs">
// Copyright (c) 2021 Nikolai Reinke. All rights reserved.
// </copyright>
// <author>
//   Nikolai Reinke
// </author>
// ------------------------------------------------------------------------------------------------------


using System.IO;
using System.Xml;
using UnityEngine;

namespace DIDemo.MovieExample
{
    class MovieXMLDatabaseService : IMovieDatabaseService
    {
        public void FetchMovie(string movieName)
        {
            XmlNodeList xmlNodes = GetMoviesFromXML();

            for (int i = 0; i < xmlNodes.Count; i++)
            {
                if (xmlNodes.Item(i).InnerText == movieName)
                {
                    Debug.Log("Was able to provide movie " + movieName + " from XML.");
                    return;
                }
            }

            Debug.Log("Was not able to provide movie " + movieName + ".");
        }

        private XmlNodeList GetMoviesFromXML()
        {
            XmlDataDocument xmldoc = new XmlDataDocument();

            FileStream fs = new FileStream(Application.dataPath + "/Resources/MovieLibrary.xml", FileMode.Open,
                FileAccess.Read);

            xmldoc.Load(fs);
            return (xmldoc.GetElementsByTagName("title"));
        }
    }
}