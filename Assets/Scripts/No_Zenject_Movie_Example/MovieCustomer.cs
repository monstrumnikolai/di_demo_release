﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace DIDemo.MovieExample
{

    public class MovieCustomer : ICustomer
    {
        IMovieStore _movieStore;

        public MovieCustomer(IMovieStore movieStore)
        {
            _movieStore = movieStore;
        }

        public void RequestMovie(string movieName)
        {
            Debug.Log("Customer is asking for movie " + movieName);
            _movieStore.GetMovieByName(movieName);
        }
    }
}

