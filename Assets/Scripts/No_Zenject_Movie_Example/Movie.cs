﻿// ------------------------------------------------------------------------------------------------------
// <copyright file="Movie.cs" company="Monstrum">
// Copyright (c) 2021 Monstrum. All rights reserved.
// </copyright>
// <author>
//   Nikolai Reinke
// </author>
// ------------------------------------------------------------------------------------------------------

namespace DIDemo.MovieExample
{
    public class Movie
    {
        public string Name { get; private set; }

        public Movie(string name)
        {
            Name = name;
        }
    }
}