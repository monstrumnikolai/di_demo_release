﻿namespace DIDemo.MovieExample
{
    public interface IMovieStore
    {
        void GetMovieByName(string moviename);
    }
}