﻿// ------------------------------------------------------------------------------------------------------
// <copyright file="ListMovieFetchService.cs">
// Copyright (c) 2021 Nikolai Reinke. All rights reserved.
// </copyright>
// <author>
//   Nikolai Reinke
// </author>
// ------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

namespace DIDemo.MovieExample
{
    public class MovieListDatabaseService : IMovieDatabaseService
    {
        // Probably not the best data structure to use, but since this is just an example implementation.
        private List<Movie> _movies = new List<Movie>()
        {
            new Movie("Inception"),
            new Movie("Titanic"),
            new Movie("Avatar"),
            new Movie("Memento"),
            new Movie("Bambi"),
        };
        
        public void FetchMovie(string movieName)
        {
            foreach (Movie movie in _movies)
            {
                if (movie.Name == movieName)
                {
                    Debug.Log("Was able to provide movie " + movieName + " from internal list.");
                    return;
                }
            }

            Debug.Log("Was not able to provide movie " + movieName + " from internal list.");
        }
    }
}